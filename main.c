#define USE_FULL_ASSERT

#include "sims.h"

int main(char* argv[], int argc)
{
	int option = 0; /* 用于记录用户选项 */
	student_t* stu_list = NULL;
	user_t* user_list = NULL;
	user_t user;
	memset(&user, 0, sizeof(user));

	FILE* fp = fopen("E:\\project\\c\\my_sims\\configure.txt", "r"); /* 打开配置文件 */
	fseek(fp, 0, SEEK_SET); /* 文件指针移至文件首 */
	char user_path[128] = "";
	char stu_path[128] = "";
	char str[128] = "";

	/* 获取配置文件信息 */
	fgets(str, sizeof(str), fp); /* 读取一行 */
	strncpy(user_path, str, strlen(str) - 1);  /* 不拷贝换行符 */
	memset(str, 0, sizeof(str)); /* 清空str防止脏数据 */
	fgets(str, sizeof(str), fp);
	if (str[strlen(str)] == '\n')
	{
		strncpy(stu_path, str, strlen(str) - 1); /* 如果有换行符，不拷贝换行符 */
	}
	else
	{
		strncpy(stu_path, str, strlen(str)); /* 无换行符全部拷贝 */
	}
	/*	测试打印
	printf("%zd\n", strlen(str));
	printf("user_path : %s\n", user_path);
	printf("stu_path : %s\n", stu_path);*/

	/* 系统初始化 */
	system_init(&user_list, &stu_list, user_path, stu_path);

	/* 主循环 */
	while (1)
	{
		system("cls");  /* 清屏 */
		menu_init();
		printf("Please enter your number: ");
		option = getchar() - '0';  /* 获取输入的字符并转为数字 */
		
		if (2 == option)
		{
			system_exit();
			return 0;
		}
		else if (LEGAL_LEVEL(option))
		{
			user.level = option;
		}
		else
		{
			fflush(stdin); /* 刷新输入缓冲区 */
			printf("Input error!\n");
			assert_param(LEGAL_LEVEL(option)); /* 使用断言打印位置 */
			printf("Press any key to re-enter...");
			_getch(); /* 接受任意字符后继续运行，getch()需要按Enter键才能继续运行 */
			continue;
		}

		system("cls");
		printf("Please enter the user name and password...\n");
		printf("user name: ");
		scanf("%s", &user.name);
		printf("password : ");
		fflush(stdin);
		get_passwoed(user.password);
		if (search_user(user_list, user))
		{ 
			/* 在用户文件中找到了输入用户 */
			if (ADMIN == user.level)
			{
				admin_function_select(&user_list, &stu_list, user_path, stu_path);
			}
			else if (NORMAL == user.level)
			{
				normal_function_select(stu_list);
			}
			else
			{
				printf("user level error...\n");
			}
			break;
		}
		else
		{
			printf("Not found user!\n");
			fflush(stdin);
			printf("Please enter 'y' to login re-enter, or any other key to exit...\n");
			if ('y' != _getch())
			{
				break;
			}
		}
	}

	/*system("pause");*/
	return 0;
}