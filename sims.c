#include "sims.h"

/*
* 函数功能：用于断言，打印错误位置
* 函数参数：
*			file:文件名
*			line:行号
*/
void assert_failed(unsigned char* file, unsigned int line)
{
	printf("error informaation:\nfile: %s , line: %d \n", file, line);
}

/*
* 函数功能：系统初始化
* 函数参数：
*			user_list:	用户
*			stu_list:	学生
*			user_path:	用户信息文件路径
*			stu_path:	学生信息文件路径
*/
void system_init(user_t** user_list, student_t** stu_list, char* user_path, char* stu_path)
{
	FILE* fp_user = fopen(user_path, "r+");
	fseek(fp_user, 0, SEEK_SET);
	FILE* fp_stu = fopen(stu_path, "r+");
	fseek(fp_stu, 0, SEEK_SET);

	user_t* user_head = (user_t*)malloc(sizeof(user_t)); /* malloc 不会初始化 */
	memset(user_head, 0, sizeof(user_t));
	*user_list = user_head; /* 传出参数 */
	student_t* stu_head = (student_t*)calloc(1, sizeof(student_t)); /* 分配1个student_t大小，并初始化 */
	*stu_list = stu_head;

	user_t user, * puser = NULL, * pcuser = NULL;
	memset(&user, 0, sizeof(user));
	pcuser = user_head;
	student_t stu, * pstu = NULL, * pcstu = NULL, * prestu = NULL, * pcurstu = NULL;
	memset(&stu, 0, sizeof(stu));
	pcstu = stu_head;

	if (NULL != fp_user)
	{
		while (EOF != (fscanf(fp_user, "%s%s%d", &user.name, &user.password, &user.level))) /* 将文件行格式化打印到变量user */
		{
			/* 每得到一条信息，分配一个节点并插入链表 */
			puser = (user_t*)calloc(1, sizeof(user_t)); /* 含有指针最好用calloc，因为它会自动初始化 */
			strcpy(puser->name, user.name);
			strcpy(puser->password, user.password);
			puser->level = user.level;

			/* 使用尾插法插入节点 */
			if (NULL == user_head->next) /* 插入第一个节点 */
			{
				puser->next = NULL;
				user_head->next = puser;
				pcuser = puser; /* 指向当前节点 */
			}
			else
			{
				pcuser->next = puser; /* 插入节点 */
				pcuser = puser; /* 指针下移 */
			}

			memset(&user, 0, sizeof(user));
		}
		fclose(fp_user);
	}
	else
	{
		perror("fopen user configure file error");
	}

	if (NULL != fp_stu)
	{
		while (EOF != (fscanf(fp_stu, "%d%s", &stu.id, &stu.name)))
		{
			/* 获取文件中的记录 */
			pstu = (student_t*)calloc(1, sizeof(student_t)); /* 新分配节点必须初始化，否则next可能成为野指针 */
			pstu->id = stu.id;
			strcpy(pstu->name, stu.name);
			for (int i = 0; i < COURSE_NUM; i++)
			{
				fscanf(fp_stu, "%d%lf", &pstu->course[i].id, &pstu->course[i].score); /* 获取课程号及成绩 */
			}

			/* 插入节点 */
			if (NULL == stu_head)
			{
				/* 插入第一个节点 */
				pstu->next = NULL;
				stu_head->next = pstu;
				pcstu = pstu;
			}
			else
			{
				prestu = stu_head;
				pcurstu = stu_head->next;
				while (NULL != pcurstu) /* 不是尾节点 */
				{
					/* 按id从小到大插入链表 */
					if (pcurstu->id > pstu->id)
					{
						pstu->next = pcurstu;
						prestu->next = pstu;
						break;
					}
					else
					{
						pcurstu = pcurstu->next; /* 指针下移 */
						prestu = prestu->next;
					}
				}

				if (NULL == pcurstu)
				{
					/* 尾部插入 */
					pcstu->next = pstu;
					pcstu = pstu;
				}
			}
		}
		fclose(fp_stu);
	}
	else
	{
		perror("fopen student configure file error");
	}
}

/*
* 函数功能：菜单初始化
* 函数参数：
*			无
*/
void menu_init()
{
	printf("--------------------------------------------------\n");
	printf("|               Welcome to SIMS!                 |\n");
	printf("|         -----------------------------          |\n");
	printf("| Please enter your login level:                 |\n");
	printf("|                                                |\n");
	printf("|    [0] You are a teacher                       |\n");
	printf("|    [1] You are a student                       |\n");
	printf("|    [2] Exit                                    |\n");
	printf("--------------------------------------------------\n");
}

/*
* 函数功能：管理员菜单初始化
* 函数参数：
*			无
*/
void admin_menu_init()
{
	printf("--------------------------------------------------\n");
	printf("| [1] %-8s student information               |\n", "search");
	printf("| [2] %-8s student information               |\n", "add");
	printf("| [3] %-8s student information               |\n", "update");
	printf("| [4] %-8s student information               |\n", "delete");
	printf("| [5] %-8s user acount                       |\n", "search");
	printf("| [6] %-8s user acount                       |\n", "add");
	printf("| [7] %-8s user acount                       |\n", "update");
	printf("| [8] %-8s user acount                       |\n", "delete");
	printf("| [9] exit                                       |\n");
	printf("--------------------------------------------------\n");
}

/*
* 函数功能：普通账户菜单初始化
* 函数参数：
*			无
*/
void normal_menu_init()
{
	printf("--------------------------------------------------\n");
	printf("| [1] %-7s student information                |\n", "search");
	printf("| [2] exit                                       |\n");
	printf("--------------------------------------------------\n");
}

/*
* 函数功能：获取键盘输入的密码，并在用户界面加密显示为 *
* 函数参数：
*			password: 用于接收输入的密码，由调用程序分匹配内存	
*/
void get_passwoed(char* password)
{
	int index = 0;
	char ch;
	while ((ch = _getch()) != '\r') /* 当输入Enter时，结束密码输入，\r表示回车 */
	{
		if (ch != '\b')
		{
			printf("*");
			password[index++] = ch; /* 接收键入字符 */
		}
		else /* 如果输入退格键Backspace，则左移*/
		{
			printf("\b \b"); /* 退格显示，
								为什么要"\b \b"，因为\b只是退格，不会删除字符，
								在\b退格后打印一个空格来覆盖原本的字符*，然后再\b退格，
								这样在视觉上就好像删除掉了一个字符，并且退格后可以继续输入，
								如果仅printf("\b")，仅仅会退格，单字符还是会显示出来，可以自己测试*/
			index--;
		}
		password[index] = '\0'; /* 使符合C字符串格式 */
	}
}

/*
* 函数功能：检索用户是否存在
* 函数参数：
*			user_list: 用户链表
*			user: 输入的用户
* 函数返回：找到返回1，失败返回0
*/
int search_user(user_t* user_list, user_t user) 
{
	while (NULL != user_list->next)
	{
		if ((0 == strcmp(user_list->next->name, user.name)) &&
			(0 == strcmp(user_list->next->password, user.password)))
		{
			return 1; /* 找到返回1 */
		}
		user_list->next = user_list->next->next;
	}
	
	return 0;
}

/*
* 函数功能：管理员功能选择
* 函数参数：
*			user_adr: 用户指针的地址
*			stu_adr: 学生指针的地址
*			user_path: 用户文件的路径
*			stu_path: 学生文件的路径
*/
void admin_function_select(user_t** user_adr, student_t** stu_adr, char* user_path, char* stu_path)
{
	char ch = 0; /* 键盘输入的都是字符 */

	while (1)
	{
		system("cls");
		admin_menu_init();
		printf("Please enter your number: ");
		fflush(stdin);
		scanf("%c", &ch);
		switch (ch)
		{
		case '1':
			search_student_information(*stu_adr, ADMIN);
			break;
		case '2':
			add_student(stu_adr, stu_path);
			break;
		case '3':
			update_student(stu_adr, stu_path);
			break;
		case '4':
			delete_student(stu_adr, stu_path);
			break;
		case '5':
			search_user_infomation(*user_adr);
			break;
		case '6':
			add_user(user_adr, user_path);
			break;
		case '7':
			update_user(user_adr, user_path);
			break;
		case '8':
			delete_user(user_adr, user_path);
			break;
		case '9':
			system_exit();
			break;
		case '\n':
			break;
		default:
			fflush(stdin);
			printf("Input error!\n Press any key to re-enter...\n");
			_getch();
			break;
		}
	}
}

/*
* 函数功能：普通用户功能选择
* 函数参数：
*			stu: 学生指针
*/
void normal_function_select(student_t* stu)
{
	char ch;

	while (1)
	{
		system("cls");
		normal_menu_init();
		printf("Please enter your number: ");
		fflush(stdin);
		scanf("%c", &ch);
		switch (ch)
		{
		case '1':
			search_student_information(stu, NORMAL);
			break;
		case '2':
			system_exit();
			break;
		case '\n':
			break;
		default:
			fflush(stdin);
			printf("Input error!\n Press any key to re-enter...\n");
			_getch();
			break;
		}
	}
}

/*
* 函数功能：查询学生信息
* 函数参数：
*			stu: 学生的指针
*			level: 用户的级别
*/
void search_student_information(student_t* stu, int level)
{
	char ch;

	while (1)
	{
		system("cls");
		search_mode_init(level);
		printf("Please enter your number: ");
		fflush(stdin);
		scanf("%c", &ch);
		switch (ch)
		{
		case '0':
			if (ADMIN == level)
			{
				search_stu_all(stu);
			}
			break;
		case '1':
			search_by_stu_id(stu);
			break;
		case '2':
			search_by_stu_name(stu);
			break;
		case '3':
			return;
			break;
		case '\n':
			break;
		default:
			fflush(stdin);
			printf("Input error!\n Press any key to re-enter...\n");
			_getch();
			break;
		}
	}
}

/*
* 函数功能：查询方式菜单初始化
* 函数参数：
*			level: 用户的权限级别
*/
void search_mode_init(int level)
{
	printf("--------------------------------------------------\n");
	if (ADMIN == level)
	{
		printf("| [0] search all information                     |\n");
	}
	printf("| [1] search by id                               |\n");
	printf("| [2] search by name                             |\n");
	printf("| [3] return                                     |\n");
	printf("--------------------------------------------------\n");
}

/*
* 函数功能：通过学生id查找
* 函数参数：
*			stu: 学生链表指针
*/
void search_by_stu_id(student_t* stu)
{
	int id;
	student_t* pc = NULL;
	if (NULL != stu)
	{
		pc = stu->next;
	}
	else
	{
		printf("parameter error!\n");
		goto RT_SEARCH_BY_STU_ID;
	}

	system("cls");
	printf("Enter student id: ");
	scanf("%d", &id);

	while (NULL != pc)
	{
		if (pc->id == id)
		{
			printf("%4d\t%-8s\t", pc->id, pc->name);
			for (int i = 0; i < COURSE_NUM; i++)
			{
				printf("%5d  %-8.2f  ", pc->course[i].id, pc->course[i].score);
			}
			printf("\n");
			/* 找到了，跳出循环 */
			/* break; */
			goto RT_SEARCH_BY_STU_ID;
		}
		else
		{
			/* 没找到，指针后移 */
			pc = pc->next;
		}
	}
	printf("No student with id %d was found...\n", id);

RT_SEARCH_BY_STU_ID:
	printf("\nPlease enter any key to continue...\n");
	fflush(stdin);
	_getch();
}

/*
* 函数功能：通过学生name查找
* 函数参数：
*			stu: 学生链表指针
*/
void search_by_stu_name(student_t* stu)
{
	char name[NAME_LEN] = { 0 };
	student_t* pc = NULL;
	if (NULL == stu)
	{
		pc = stu->next;
	}
	else
	{
		printf("parameter error\n");
		goto RT_SEARCH_BY_STU_NAME;
	}

	system("cls");
	printf("Enter student name: ");
	scanf("%s", &name);

	while (NULL != pc)
	{
		if (0 == strcmp(name, pc->name))
		{
			printf("%4d\t%-8s\t", pc->id, pc->name);
			for (int i = 0; i < COURSE_NUM; i++)
			{
				printf("%5d  %-8.2f  ", pc->course[i].id, pc->course[i].score);
			}
			printf("\n");
			/* 找到了，跳出循环 */
			/* break; */
			goto RT_SEARCH_BY_STU_NAME;
		}
		else
		{
			/* 没找到，指针后移 */
			pc = pc->next;
		}
	}
	printf("No student named %s was found...\n", name);

RT_SEARCH_BY_STU_NAME:
	printf("\nPlease enter any key to continue...\n");
	fflush(stdin);
	_getch();
}

/*
* 函数功能：查询所有学生信息
* 函数参数：
*			stu: 学生链表指针
*/
void search_stu_all(student_t* stu)
{
	student_t* pc = NULL;
	if (NULL != stu)
	{
		pc = stu->next;
	}

	system("cls");

	while (NULL != pc)
	{
		printf("%4d\t%-8s\t", pc->id, pc->name);
		for (int i = 0; i < COURSE_NUM; i++)
		{
			printf("%5d  %-8.2f  ", pc->course[i].id, pc->course[i].score);
		}
		printf("\n");
		pc = pc->next;
	}

	printf("\nPlease enter any key to continue...\n");
	fflush(stdin);
	_getch();
}

/*
* 函数功能：增加学生
* 函数参数：
*			stu: 学生链表指针的地址
*			path: 文件路径
*/
void add_student(student_t** stu, char* path)
{
	student_t* pre = NULL, * pcur = NULL;
	student_t* ptem = (student_t*)calloc(1, sizeof(student_t));

	while (1) /* 可以连续增加学生 */
	{
		system("cls");
		printf("Please enter student information: [id name] and %d course [id score]\n", COURSE_NUM);
		printf("student-id student-name\ncourse-id course-score\n...\n");
		printf("--------------------------------------------------------------------------\n");
		fflush(stdin);

		/* 输入学生id和name */
		scanf("%d%s", &ptem->id, &ptem->name);
		/* 输入该学生的课程id及成绩 */
		for (int i = 0; i < COURSE_NUM; i++)
		{
			scanf("%d%lf", &ptem->course[i].id, &ptem->course[i].score);
		}

		/* 开始插入 */
		if (NULL == (*stu)->next)
		{
			/* 链表中没有其他节点，直接插入尾部 */
			ptem->next = NULL;
			(*stu)->next = ptem;

			/* 插入成功，更新配置文件内容 */
			printf("Success!\n");
			update_student_database(stu, path);
		}
		else
		{
			/* 根据学生id按顺序插入，id从小到大 */
			pre = (*stu);
			pcur = (*stu)->next;
			while (NULL != pcur)
			{
				if (pcur->id > ptem->id)
				{
					ptem->next = pcur;
					pre->next = ptem;

					/* 插入成功，更新配置文件内容 */
					printf("Success!\n");
					update_student_database(stu, path);
					break;
				}
				else if (pcur->id == ptem->id)
				{
					/* id已存在，不插入 */
					printf("Error: this student id already exists...\n");
					break;
				}
				else
				{
					pre = pre->next;
					pcur = pcur->next;
				}
			}
			if (NULL == pcur)
			{
				/* 待插入节点id最大，在尾部插入 */
				pre->next = ptem;

				/* 插入成功，更新配置文件内容 */
				printf("Success!\n");
				update_student_database(stu, path);
			}
		}

		printf("Please enter 'y' to continue adding students, or enter any key to return...\n");
		fflush(stdin);
		if ('y' != _getch())
		{
			break;
		}
	}
}

/*
* 函数功能：更新学生数据库配置文件
* 函数参数：
*			stu: 学生链表指针的地址
*			path: 文件路径
*/
void update_student_database(student_t** stu, char* path)
{
	FILE* fp = fopen(path, "w");
	student_t* pc = NULL;
	if (stu != NULL)
	{
		pc = (*stu)->next;
	}

	if (NULL != fp)
	{
		while (NULL != pc)
		{
			/* 打印到文件 */
			fprintf(fp, "%4d\t%-8s\t", pc->id, pc->name);
			for (int i = 0; i < COURSE_NUM; i++)
			{
				fprintf(fp, "%5d  %-8.2f  ", pc->course[i].id, pc->course[i].score);
			}
			fprintf(fp, "\n");

			/* 指针后移 */
			pc = pc->next;
		}

		fclose(fp);
	}
	else
	{
		printf("fopen(%s) error!\n", path);
	}
}

/*
* 函数功能：更新学生信息
* 函数参数：
*			stu: 学生指针的地址
*			path: 文件路径
*/
void update_student(student_t** stu, char* path)
{
	int id = 0, flag = 0;
	student_t* pc = NULL;
	if (NULL != stu)
	{
		pc = (*stu)->next;
	}

	system("cls");
	printf("Please enter id: ");
	scanf("%d", &id);

	while (NULL != pc)
	{
		/* 根据id查找 */
		if (id == pc->id)
		{
			/* 找到了，输入更新信息 */
			printf("Please enter student information: [id name] and %d course [id score]\n", COURSE_NUM);
			printf("student-id student-name\ncourse-id course-score\n...\n");
			printf("--------------------------------------------------------------------------\n");
			
			scanf("%d%s", &pc->id, &pc->name);
			for (int i = 0; i < COURSE_NUM; i++)
			{
				scanf("%d%lf", &pc->course[i].id, &pc->course[i].score);
			}

			flag = 1; /* 找到该id标志置为1 */
			break;
		}
		else
		{
			pc = pc->next;
		}
	}

	if (0 == flag)
	{
		printf("Error: not found this student...\n");
	}
	else
	{
		printf("Success!\n");
		update_student_database(stu, path); /* 更新数据库文件 */
	}

	printf("Please enter any key to continte...\n");
	fflush(stdin);
	_getch();
}

/*
* 函数功能：删除学生
* 函数参数：
*			stu: 学生指针的地址
*			path: 文件路径
*/
void delete_student(student_t** stu, char* path)
{
	int id = 0, flag = 0;
	student_t* pre = NULL, * pcur = NULL;
	if (NULL != stu)
	{
		pre = (*stu);
		pcur = pre->next;
	}

	system("cls");
	printf("Please enter the id of the student you want to delete: \n");
	scanf("%d", &id);

	while (NULL != pcur)
	{
		if (pcur->id == id)
		{
			/* 找到了，从链表中删除节点并释放内存 */
			pre->next = pcur->next;
			free(pcur);
			pcur = NULL;
			flag = 1; /* 找到了 */
			break;
		}
		else
		{
			pre = pre->next;
			pcur = pcur->next;
		}
	}

	if (!flag)
	{
		printf("Error: not found this student...\n");
	}
	else
	{
		printf("Success!\n");
		update_student_database(stu, path); /* 更新数据库文件 */
	}

	printf("Please enter any key to continue...\n");
	fflush(stdin);
	_getch();
}

/*
* 函数功能：查询登录用户信息
* 函数参数：
*			user: 用户链表
*/
void search_user_infomation(user_t* user)
{
	char ch = 0;
	while (1)
	{
		system("cls");
		search_menu_init();
		printf("Please enter your number: ");
		fflush(stdin);
		scanf("%c", &ch);
		switch (ch)
		{
		case '0':
			search_user_all(user);
			break;
		case '1':
			search_by_user_level(user);
			break;
		case '2':
			search_by_user_name(user);
			break;
		case '3':
			return;
			break;
		case '\n':
			break;
		default:
			fflush(stdin);
			printf("Input error!\n Press any key to re-enter...\n");
			_getch();
			break;
		}
	}
}

/*
* 函数功能：用户菜单
* 函数参数：
*			无
*/
void search_menu_init()
{
	printf("--------------------------------------------------\n");
	printf("| [0] search all information                     |\n");
	printf("| [1] search by level                            |\n");
	printf("| [2] search by name                             |\n");
	printf("| [3] return                                     |\n");
	printf("--------------------------------------------------\n");
}

/*
* 函数功能：查询所有用户信息
* 函数参数：
*			user: 用户链表的指针
*/
void search_user_all(user_t* user)
{
	user_t* pc = NULL;
	if (NULL != user)
	{
		pc = user->next;
	}

	system("cls");
	while (NULL != pc)
	{
		if (pc->level == ADMIN)
		{
			printf("name: %-20s, password: %-10s, level: %s\n", pc->name, pc->password, "ADMIN");
		}
		else
		{
			printf("name: %-20s, password: %-10s, level: %s\n", pc->name, pc->password, "NORMAL");
		}
		pc = pc->next;
	}

	printf("Please enter any key to continue...\n");
	fflush(stdin);
	_getch();
}

/*
* 函数功能：通过用户登录级别查询
* 函数参数：
*			user: 用户链表的指针
*/
void search_by_user_level(user_t* user)
{
	int lev = 0;
	user_t* pc = NULL;
	if (NULL != user)
	{
		pc = user->next;
	}

	/* 输入0或1 */
	system("cls");
	printf("Please enter lenel: 0-ADMIN, 1-NORMAL\n");
	scanf("%d", &lev);

	if (!LEGAL_LEVEL(lev))
	{
		/* 输入错误 */
		system("cls");
		printf("Error: please enter 0 or 1.\n");
	}
	else
	{
		while (NULL != pc)
		{
			if (ADMIN == pc->level && ADMIN == lev)
			{
				printf("name: %-20s, password: %-10s, level: %s\n", pc->name, pc->password, "ADMIN");
			}
			else if (NORMAL == pc->level && NORMAL == lev)
			{
				printf("name: %-20s, password: %-10s, level: %s\n", pc->name, pc->password, "NORMAL");
			}
			else
			{
				;
			}
			pc = pc->next;
		}
	}

	printf("Please enter any key to continue...\n");
	fflush(stdin);
	_getch();
}

/*
* 函数功能：通过用户名查询
* 函数参数：
*			user: 用户链表的指针
*/
void search_by_user_name(user_t* user)
{
	char name[NAME_LEN] = "";
	user_t* pc = NULL;
	if (NULL != user)
	{
		pc = user->next;
	}

	system("cls");
	printf("Enter user name: ");
	scanf("%s", &name);

	while (NULL != pc)
	{
		if (!strcmp(name, pc->name))
		{
			/* 找到了 */
			if (ADMIN == pc->level)
			{
				printf("name: %-20s, password: %-10s, level: %s\n", pc->name, pc->password, "ADMIN");
			}
			else
			{
				printf("name: %-20s, password: %-10s, level: %s\n", pc->name, pc->password, "NORMAL");
			}
			break;
		}
		else
		{
			pc = pc->next;
		}
	}

	if (NULL == pc)
	{
		printf("No user named %s was found...\n", name);
	}

	printf("Please enter any key to continue...\n");
	fflush(stdin);
	_getch();
}

/*
* 函数功能：增加用户
* 函数参数：
*			user: 用户指针的地址
*			path: 用户文件的路径
*/
void add_user(user_t** user, char* path)
{
	user_t* ptem = (user_t*)malloc(sizeof(user_t));
	memset(ptem, 0, sizeof(user_t));

	user_t* pc = NULL;
	if (NULL != user)
	{
		pc = (*user)->next;
	}

	while (1) /* 支持循环输入 */
	{
		system("cls");
		printf("Please enter user information: [name] [password] [level]\n");
		printf("user-name\nuser-password\nuser-level\n");
		printf("level: 0-ADMIN, 1-NORMAL\n");
		printf("--------------------------------------------------------------------------\n");
		fflush(stdin);

		/* 输入 */
		scanf("%s%s%d", &ptem->name, &ptem->password, &ptem->level);

		/* 检查输入的用户名是否已存在 */
		while (NULL != pc)
		{
			if (!strcmp(ptem->name, pc->name))
			{
				printf("Error: this user name already exists...\n");
				goto RT_ADD_USER;
			}
			pc = pc->next;
		}

		/* 插入 */
		ptem->next = (*user)->next;
		(*user)->next = ptem;

		printf("Success!\n");
		update_user_database(user, path);

	RT_ADD_USER:
		printf("Please enter 'y' to continue adding students, or enter any key to return...\n");
		fflush(stdin);
		if ('y' != _getch())
		{
			break;
		}
	}
}

/*
* 函数功能：更新用户
* 函数参数：
*			user: 用户指针的地址
*			path: 用户文件的路径
*/
void update_user(user_t** user, char* path)
{
	int flag = 0;
	char name[NAME_LEN] = "";
	user_t* pc = NULL;
	if (NULL != user)
	{
		pc = (*user)->next;
	}

	system("cls");
	printf("Enter user name: ");
	scanf("%s", &name);

	while (NULL != pc)
	{
		if (!strcmp(name, pc->name))
		{
			/* 找到了 */
			system("cls");
			printf("Please enter user information: [name] [password] [level]\n");
			printf("user-name\nuser-password\nuser-level\n");
			printf("level: 0-ADMIN, 1-NORMAL\n");
			printf("--------------------------------------------------------------------------\n");
			fflush(stdin);
			scanf("%s%s%d", &pc->name, &pc->password, &pc->level);
			flag = 1;
			break;
		}
		else
		{
			pc = pc->next;
		}
	}

	if (0 == flag)
	{
		printf("Error: not found this user...\n");
	}
	else
	{
		/* 成功，更新数据库 */
		printf("Success!\n");
		update_user_database(user, path);
	}

	printf("Please enter any key to continue...\n");
	fflush(stdin);
	_getch();
}

/*
* 函数功能：更新用户数据库
* 函数参数：
*			user: 用户指针的地址
*			path: 用户文件的路径
*/
void update_user_database(user_t** user, char* path)
{
	FILE* fp = fopen(path, "w");
	user_t* pc = NULL;
	if (user != NULL)
	{
		pc = (*user)->next;
	}

	if (fp != NULL)
	{
		while (NULL != pc)
		{
			/* 把新的用户链表打印进文件 */
			fprintf(fp, "%-20s %-10s %d\n", pc->name, pc->password, pc->level);
			pc = pc->next;
		}
	}
	else
	{
		printf("fopen(%s) error!\n", path);
	}
}

/*
* 函数功能：删除用户
* 函数参数：
*			user: 用户指针的地址
*			path: 用户文件的路径
*/
void delete_user(user_t** user, char* path)
{
	int flag = 0;
	char name[NAME_LEN] = "";
	user_t* pre = NULL, * pcur = NULL;
	if (NULL != user)
	{
		pre = (*user);
		pcur = pre->next;
	}

	system("cls");
	printf("Please enter the name of the user you want to delete: \n");
	scanf("%s", &name);

	while (NULL != pcur)
	{
		if (!strcmp(name, pcur->name))
		{
			/* 删除 */
			pre->next = pcur->next;
			free(pcur);
			pcur = NULL;
			flag = 1;
			break;
		}
		else
		{
			pcur = pcur->next;
			pre = pre->next;
		}
	}

	if (0 == flag)
	{
		printf("Error: not found this user...\n");
	}
	else
	{
		/* 成功，更新数据库 */
		printf("Success!\n");
		update_user_database(user, path);
	}

	printf("Please enter any key to continue...\n");
	fflush(stdin);
	_getch();
}

/*
* 函数功能：退出并打印感谢使用信息
* 函数参数：
*			无
*/
void system_exit()
{
	system("cls"); /* 清屏 */

	printf("---------------------------------------------------\n");
	printf("|            Thanks for using SIMS!               |\n");
	printf("---------------------------------------------------\n");

	system("pause");
	exit(1);
}

