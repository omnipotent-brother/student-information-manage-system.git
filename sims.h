#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>

#define NAME_LEN		21
#define PASSWORD_LEN	16
#define COURSE_NUM		3

/* 
* 用户结构体 
*/
typedef struct user
{
	char name[NAME_LEN]; /* 用户名称 */
	char password[PASSWORD_LEN]; /* 用户密码 */
	int level; /* 权限级别 */
	struct user* next;
}user_t; 

/*
* 课程结构体
*/
typedef struct course
{
	int id; /* 课程号 */
	double score; /* 成绩，尽量使用double，float容易失真 */
}course_t;

/*
* 学生结构体
*/
typedef struct student
{
	int id;
	char name[NAME_LEN];
	course_t course[COURSE_NUM];
	struct student* next;
}student_t;

/*
* 账户级别：admin管理员账户，normal普通账户
*/
enum{ADMIN = 0, NORMAL}; 
#define LEGAL_LEVEL(level) ((ADMIN == level) || \
							(NORMAL == level))
/*
* 通过断言判定是否合法，即是否在enum枚举范围内
*/
#ifdef USE_FULL_ASSERT
void assert_failed(unsigned char* file, unsigned int line);
#define assert_param(expr) ((expr) ? (void)0 : assert_failed((unsigned char *)__FILE__, __LINE__))
#else
#define assert_param(expr) ((void)0)
#endif

/*
* API
*/
void system_init(user_t** user_list, student_t** stu_list, char* user_path, char* stu_path);

void menu_init();

void admin_menu_init();

void normal_menu_init();

void get_passwoed(char* password);

int search_user(user_t* user_list, user_t user);

void admin_function_select(user_t** user_adr, student_t** stu_adr, char* user_path, char* stu_path);

void normal_function_select(student_t* stu);

void search_student_information(student_t* stu, int level);

void search_mode_init(int level);

void search_by_stu_id(student_t* stu);

void search_by_stu_name(student_t* stu);

void search_stu_all(student_t* stu);

void add_student(student_t** stu, char* path);

void update_student_database(student_t** stu, char* path);

void update_student(student_t** stu, char* path);

void delete_student(student_t** stu, char* path);

void search_user_infomation(user_t* user);

void search_menu_init();

void search_user_all(user_t* user);

void search_by_user_level(user_t* user);

void search_by_user_name(user_t* user);

void add_user(user_t** user, char* path);

void update_user(user_t** user, char* path);

void update_user_database(user_t** user, char* path);

void delete_user(user_t** user, char* path);

void system_exit();






